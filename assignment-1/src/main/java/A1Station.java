import java.util.Scanner;
import java.util.ArrayList;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class
    public static void keberangkatan(TrainCar kereta, int total) {
    	System.out.println("The train departs to Javari Park");
    	System.out.print("[LOCO]<");
    	kereta.printCar();

    	String kondisiKucing = "";
    	double rata2Kucing = kereta.computeTotalMassIndex()/total;

    	if (rata2Kucing < 18.5) {
            kondisiKucing = "underweight";
        } else if (rata2Kucing >= 18.5 && rata2Kucing < 25) {
            kondisiKucing = "normal";
        } else if (rata2Kucing >= 25 && rata2Kucing < 30) {
            kondisiKucing = "overweight";
        } else if (rata2Kucing >= 30) {
            kondisiKucing = "obese";
        }

        System.out.println("Average mass index of all cats: " + rata2Kucing);
        System.out.println("in average, the cats in the train are " + kondisiKucing);
    }

    public static void main(String[] args) {
        // TODO Complete me!
    	Scanner input = new Scanner(System.in);
    	System.out.print("Jumlah Kucing: ");
    	int jmlh = input.nextInt();

    	int counter = 0; //counter untuk menghitung jumlah kucing
    	String[] data;
    	TrainCar kereta = null;
    	ArrayList<TrainCar> panjangKereta =  new ArrayList<>();
    	ArrayList<Integer> jumlahKucingKereta = new ArrayList<>();

    	for (int x = 0; x < jmlh ; x++) {
    		counter += 1;
    		String cat = input.nextLine();
    		data = cat.split(",");
    		WildCat kucing2 = new WildCat(data[0], Double.parseDouble(data[1]), Double.parseDouble(data[2]));

    		kereta = new TrainCar(kucing2 , kereta);

    		if (kereta.computeTotalWeight() >= THRESHOLD) {
    			panjangKereta.add(kereta);
    			jumlahKucingKereta.add(counter);

    			kereta = null;
    			counter = 0;
    		}
    	}

    	if (kereta != null) {
    		panjangKereta.add(kereta);
    		jumlahKucingKereta.add(counter);
    	}

    	for (int i = 0 ; i < panjangKereta.size() ; i++) {
    		keberangkatan(panjangKereta.get(i) , jumlahKucingKereta.get(i));
    	}








    }
}
