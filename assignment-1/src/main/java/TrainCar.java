public class TrainCar {
	public WildCat cat;
	public TrainCar kereta;
	public static final int EMPTY_WEIGHT = 20;


	public TrainCar (WildCat cat){
		this.cat = cat;
	}
	public TrainCar (WildCat cat, TrainCar kereta){
		this.cat = cat;
		this.kereta = kereta;
	}
	public void Setcat( WildCat cat){
		this.cat = cat;
	}
	public WildCat Getcat(){
		return cat ;
	}
	public void Setkereta( TrainCar kereta){
		this.kereta = kereta;
	}
	public TrainCar Getkereta(){
		return kereta;
	}
	public double computeTotalWeight(){
		if (kereta == null) {
			return EMPTY_WEIGHT + cat.Getweight();
		}
		else{
			return EMPTY_WEIGHT + cat.Getweight() + kereta.computeTotalWeight();
		}
	}

	public double computeTotalMassIndex(){
		if (kereta == null) {
			return cat.computeMassIndex();
		}
		else {
			return cat.computeMassIndex() + kereta.computeTotalMassIndex();
		}
	}

	public void printCar(){
		System.out.printf("--(%s)  " , cat.Getnama());
		if (kereta == null) {
			System.out.println("");
		}
		else {
			kereta.printCar();
		}
	}
}
